package com.yalantis.firstproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private static String image_urls[];

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;

        public ViewHolder(View v) {

            super(v);
            mImageView = (ImageView) v.findViewById(R.id.iv_image_from_url);
        }

        public void SetUrl(int i) {

            Picasso.with(mImageView.getContext()).load(image_urls[i]).resize(600, 400).into(mImageView);
        }
    }


    public RecyclerAdapter(String urls[]) {

        image_urls = urls;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.SetUrl(position);
    }

    @Override
    public int getItemCount() {

        return image_urls.length;
    }
}
