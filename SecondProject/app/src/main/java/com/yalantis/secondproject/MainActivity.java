package com.yalantis.secondproject;


import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FloatingActionButtonVisibilityListener {

    private Toolbar toolbar;
    private LinearLayout textAllAppeals;
    private LinearLayout textAppealsOnTheMap;
    private TextView textEnterAccount;

    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer) ;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textAllAppeals = (LinearLayout) findViewById(R.id.all_appeals);
        textAppealsOnTheMap = (LinearLayout) findViewById(R.id.appeals_on_the_map);
        textEnterAccount = (TextView) findViewById(R.id.enter_the_account);
        textAllAppeals.setOnClickListener(this);
        textAppealsOnTheMap.setOnClickListener(this);
        textEnterAccount.setOnClickListener(this);

        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        MainFragment fragment = new MainFragment();
        getSupportFragmentManager().beginTransaction().add (R.id.fragment_container, fragment).commit();
        setTitle(R.string.all_appeals);
    }

    @Override
    public void onClick(View view) {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    @Override
    public void onVisibilityChange(boolean isVisible) {

    }
}





