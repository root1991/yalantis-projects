package com.yalantis.secondproject;

public interface FloatingActionButtonVisibilityListener {
    void onVisibilityChange(boolean isVisible);
}