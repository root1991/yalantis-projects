package com.yalantis.secondproject;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class Fragment_3 extends Fragment {

    ArrayList<ItemData> mItemData = new ArrayList<ItemData>();
    BoxAdapter boxAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_3, container, false);

        fillData();
        boxAdapter = new BoxAdapter(getActivity(), mItemData);

        ListView lvMain = (ListView) rootView.findViewById(R.id.listView);
        lvMain.setAdapter(boxAdapter);

        return rootView;
    }

    void fillData() {
        mItemData.add(new ItemData("Демонтаж інших обектів, що входять до переліку мал...","Вул. Вадима Гетьмана 57","Кві 16, 2016","-6 днів",R.drawable.ic_trash));
        mItemData.add(new ItemData("Благоустрій та будівництво","Вул. Вадима Гетьмана 42","Кві 16, 2016","-2 днів",R.drawable.ic_issues));
        mItemData.add(new ItemData("Парк","Вул. Вадима Гетьмана 12","Кві 16, 2016","-3 днів",R.drawable.ic_issues));

    }
}
