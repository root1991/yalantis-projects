package com.yalantis.secondproject;


import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BoxAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<ItemData> objects;

    BoxAdapter(Context context, ArrayList<ItemData> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_view, parent, false);
        }

        ItemData p = getProduct(position);

        ((TextView) view.findViewById(R.id.txt_description)).setText(p.mDescription);
        ((TextView) view.findViewById(R.id.txt_address)).setText(p.mAddress);
        ((TextView) view.findViewById(R.id.txt_data)).setText(p.mData);
        ((TextView) view.findViewById(R.id.txt_expectation)).setText(p.mExpectation);
        ((ImageView) view.findViewById(R.id.icon_view)).setImageResource(p.mIconView);

        return view;
    }

    ItemData getProduct(int position) {
        return ((ItemData) getItem(position));
    }
}
